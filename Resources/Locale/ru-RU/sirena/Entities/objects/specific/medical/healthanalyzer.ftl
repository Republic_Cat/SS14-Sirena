ent-AdvancedChip = чип апгрейда анализатора здоровья
    .desc = Ключевой компонент для создания продвинутого анализатора здоровья
ent-HandheldHealthAnalyzerAdv = продвинутый анализатор здоровья
    .suffix = Заряжен
    .desc = Ручной сканер тела, способный определять жизненные показатели и реагенты внутри пациента.
ent-HandheldHealthAnalyzerAdvEmpty = { ent-HandheldHealthAnalyzerAdv }
    .suffix = Пустой
    .desc = { ent-HandheldHealthAnalyzerAdv.desc }
