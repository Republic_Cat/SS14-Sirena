using Robust.Shared.Prototypes;

namespace Content.Shared.Sirena.Clothing
{
    [RegisterComponent]
    public sealed partial class ClothingGrantComponentComponent : Component
    {
        [DataField("component", required: true)]
        [AlwaysPushInheritance]
        public ComponentRegistry Components { get; private set; } = new(); // Возможно будет крашить -.-

        [ViewVariables(VVAccess.ReadWrite)]
        public bool IsActive = false;
    }
}
