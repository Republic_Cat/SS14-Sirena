namespace Content.Shared.Sirena.Components;

public abstract partial class SharedHaloComponent : Component
{
    [DataField("path")]
    public string TexturePath = string.Empty;

    [DataField("state")]
    public string TextureState = "halo";

    [DataField("desc")]
    public string OnExamined = string.Empty;
}
